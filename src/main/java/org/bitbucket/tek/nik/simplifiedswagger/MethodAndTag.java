package org.bitbucket.tek.nik.simplifiedswagger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;

import io.swagger.annotations.Authorization;
import io.swagger.models.Tag;

public class MethodAndTag {
	public MethodAndTag() {
		super();
		
	}
	public MethodAndTag(Method method, List<Tag>  tags, Annotation matchedRequestMapping,
			Authorization[] authorizationsOnApi) {
		super();
		this.method = method;
		this.tags = tags;
		this.matchedRequestMapping = matchedRequestMapping;
		this.authorizationsOnApi=authorizationsOnApi;
	}
	private Method method;
	private List<Tag>  tags;
	private Annotation matchedRequestMapping;
	Authorization[] authorizationsOnApi;
	public Authorization[] getAuthorizationsOnApi() {
		return authorizationsOnApi;
	}
	public void setAuthorizationsOnApi(Authorization[] authorizationsOnApi) {
		this.authorizationsOnApi = authorizationsOnApi;
	}
	public List<Tag> getTags() {
		return tags;
	}
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	
	public Method getMethod() {
		return method;
	}
	public void setMethod(Method method) {
		this.method = method;
	}
	
	public Annotation getMatchedRequestMapping() {
		return matchedRequestMapping;
	}
	public void setMatchedRequestMapping(Annotation matchedRequestMapping) {
		this.matchedRequestMapping = matchedRequestMapping;
	}
	

}
